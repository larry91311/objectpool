﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;

    public static PoolManager Instance
    {
        get
        {
            if(_instance==null)
            {
                Debug.Log("NULL");
            }
            return _instance;
        }

    }

    private void Awake() {
        _instance=this;
    }


    [SerializeField]
    private GameObject _bulletPrefab;

    [SerializeField]
    private List<GameObject> _bulletPool;


    [SerializeField]
    private GameObject _bulletContainer;




    List<GameObject> GenerateBullets(int amountOfBullets)
    {
        for(int i=0;i<amountOfBullets;i++)
        {
            GameObject bullet=Instantiate(_bulletPrefab);     //生成

            bullet.transform.parent=_bulletContainer.transform;    //Hie塞進Container
            bullet.SetActive(false);       //先關掉

            _bulletPool.Add(bullet);      //塞進LIST
        }
        return _bulletPool;
    }
    // Start is called before the first frame update
    void Start()
    {
        _bulletPool=GenerateBullets(10);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject RequestBullet()
    {
        //loop through the bullet list
        foreach(var bullet in _bulletPool)            //檢視POOL裡，一有準備中的BULLET就return
        {
            if(bullet.activeInHierarchy==false)
            {
                //bullet available
                bullet.SetActive(true);
                return bullet;
            }
        }
        //if we made it to this point ...we need to generate more bullets
        GameObject newBullet=Instantiate(_bulletPrefab);
        newBullet.transform.parent=_bulletContainer.transform;              //整個LIST沒有準備中的就創造一個，塞進CONTAINER塞進POOLLIST，RETURN
        _bulletPool.Add(newBullet);
        return newBullet;
        //checking for in=active bullet
        //found one ? set it active and return it to the player
        //if no bullets available(all are turned on)
        //generate X amount of bullets and run the request bullet method


    }
}
